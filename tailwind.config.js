module.exports = {
  content: ['./index.html'],
  darkMode: 'class',
  theme: {
    extend: {},
    fontFamily: {
      'lobster': ['Lobster'],
    },
  },
  plugins: [],
}
